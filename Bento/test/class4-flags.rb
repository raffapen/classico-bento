
require 'minitest/autorun'
require 'Bento'

#-------------------------------------------------------------------------------------------

class A
	include Bento::Class
	
	def foo(a, b, c = nil, d = nil, *opt, name: nil)
		flags binding, [:f1, :f2], withdefaults: [:c, :d]
		[a, b, c, d, opt, [@f1, @f2]]
	end
end

#-------------------------------------------------------------------------------------------

class Test1 < Minitest::Test

	def test_1
		assert_equal ["a", "b", "c", "d", [], [true, nil]],   A.new.foo("a", "b", "c", "d", :f1)
		assert_equal ["a", "b", nil, nil, [], [true, true]],  A.new.foo("a", "b", :f1, :f2)
		assert_equal ["a", "b", "c", nil, [], [true, nil]],   A.new.foo("a", "b", "c", :f1)
		assert_equal ["a", "b", "c", nil, [], [true, true]],  A.new.foo("a", "b", "c", :f1, :f2)
		assert_equal ["a", "b", "c", nil, [], [nil, true]],   A.new.foo("a", "b", "c", :f2)
		assert_equal ["a", "b", nil, nil, [:f3], [nil, nil]], A.new.foo("a", "b", :f3)
	end
end

#-------------------------------------------------------------------------------------------
