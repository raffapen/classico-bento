
require 'minitest/autorun'

require 'Bento'

#-------------------------------------------------------------------------------------------

class Test1 < Minitest::Test

	#---------------------------------------------------------------------------------------

	@@hosts_yaml = <<-'END'.unindent
		"zaza:vmwx:~ubuntu-15.10":
		  surname: ~ubuntu-15.10
		  domain: zaza
		  engine: vmwx
		  vmx: d:/vms/vmwx/~ubuntu-15.10/ubuntu-15.10.vmx
		  snapshot: v1
		  nics:
		    boss:
		      dev: eno16777736
		    pri:
		      dev: eno33354992
		  credentials:
		    username: root
		    password: passwd
		  ostype: linux
		  os: ubuntu-15.10
		  arch: x64
		  tags: [template, linux, ubuntu, ubuntu-15.10]

		"vcenter:vsphere:~ubuntu-15.10":
		  surname: ~ubuntu-15.10
		  snapshot: v1
		  nics:
		    boss:
		      dev: eno16777984
		    pri:
		      dev: eno33557248
		  credentials:
		    username: root
		    password: passwd
		  ostype: linux
		  os: ubuntu-15.10
		  arch: x64
		  tags: [template, linux, ubuntu, ubuntu-15.10]

		ub15:
		  engine: vmwx
		  vmx: d:/vms/vmwx/ub15/ub15.vmx
		  credentials:
		    username: root
		    password: taligent
		  ostype: linux
		  os: ubuntu-15.10
		  arch: x64
		  tags: [linux, ubuntu, ubuntu-15.10]

		"nx1:vmwx:~fedora-23-10":
		  engine: vmwx
		  vmx: n:\vms\vmwx\~fedora-23-10\fedora-23-10.vmx
		  ostype: linux
		  os: fedora-23
		END

	@@hosts1_yaml = <<-'END'.unindent
		"nx1:vmwx:~fedora-23-10":
		  engine: vmwx
		  vmx: n:\vms\vmwx\~fedora-23-10\fedora-23-10.vmx
		  ostype: linux
		  os: fedora-23
		  variant: custom
		END

	#---------------------------------------------------------------------------------------

	def self.get_nash(db, table, name, key)
		r = db.one("select json from #{table.to_s} where name=?;", name.to_s)[0]
		return r if !key
		r = JSON.parse(r)[key]
		t = db.one("select #{key.to_s} from #{table.to_s} where name=?;", name.to_s)[0]
		raise "incompatible nash" if r != t
		r
	end

	#---------------------------------------------------------------------------------------

	def test_1
		bb
		db = Bento::SQLite::DB.create()
		hosts_n = db.nash(:hosts)
		hosts = YAML.load(@@hosts_yaml)
		hosts_n.set(hosts)
		assert_equal hosts, hosts_n.all

		ub15 = hosts_n[:ub15]
		arch = Test1.get_nash(db, "hosts", "ub15", "arch")
		assert_equal "x64", ub15["arch"]
		assert_equal arch, ub15["arch"]
		assert_equal arch, ub15.arch

		ub15["arch"] = "arm"
		assert_equal "arm", ub15["arch"]
		assert_equal "arm", ub15.arch
		ub15.update!
		arch = Test1.get_nash(db, "hosts", "ub15", "arch")
		assert_equal arch, hosts_n[:ub15].arch

		ub15 = hosts_n[:ub15]
		ub15.arch = "x86"
		assert_equal "x86", ub15["arch"]
		assert_equal "x86", ub15.arch
		arch = Test1.get_nash(db, "hosts", "ub15", "arch")
		assert_equal arch, hosts_n[:ub15].arch

		ub15 = hosts_n[:ub15]
		hosts["ub15"] = "arm"
		ub15.set!(hosts["ub15"])
		arch = Test1.get_nash(db, "hosts", "ub15", "arch")
		assert_equal "arm", arch

		hosts1 = YAML.load(@@hosts1_yaml)
		hosts_n.upsert(hosts1)
		assert_equal "custom", hosts_n["nx1:vmwx:~fedora-23-10"].variant
	end
end
