
require 'Bento'
require 'minitest/autorun' 

class K < Bento::SimpleValue
	def initialize(x)
		super(x)
	end
end

class T1 < Minitest::Test

	def test_1
		assert_equal 100, K.new(10)*10
		assert_equal "10x", K.new("10").to_s + "x"
		assert_equal "abcd", K.new("ab") + "cd"
		assert_equal 30, K.new("10") + K.new("20")
		assert_equal true, K.new(false) == !K.new("true")
		assert_equal -10, -K.new(10)
	end

	def test_2
		k = K.new(10)
		assert_equal 11, k+1
		assert_equal 11, 1+k
		assert_equal true, k < 30
		assert_equal true, k >= 5
	end
end
